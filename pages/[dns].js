import {
  cvToHex,
  cvToValue,
  parseReadOnlyResponse,
  stringAsciiCV,
} from "@stacks/transactions";
import Head from "next/head";

import ProfileNotFound from "../components/public/profileNotFound";
import PublicProfile from "../components/public/publicProfile";
import {
  getContractName,
  getContractOwner,
  getStacksAPIPrefix,
} from "../services/auth";
import { convertToPersonScheme } from "../services/utils";

export default function OwlLink(data) {
  const { dns, profileFound, personSchema } = data;

  if (!profileFound) {
    return (
      <>
        <Head>
          <title>Page not found | OwlLink</title>
          <meta
            name="description"
            content="Showcase your collectibles, NFTs, and social profiles. Everything is in one place."
          />
        </Head>

        <ProfileNotFound dns={dns} />
      </>
    );
  }

  const { name, image, description } = data.data;
  const displayName = name || `@${dns}`;
  const displayImg = image || "https://owl.link/images/logo/owllink.png";
  const displayURL = `https://owl.link/${dns}`;

  return (
    <>
      <Head>
        <title>{`${displayName} | Owl Link`}</title>
        <meta name="description" content={description} />

        <meta name="robots" content="index,follow" />

        {/* Favicon */}
        <link rel="icon" href={image || "/favicon.ico"} />

        {/* Facebook Meta Tags */}
        <meta property="og:type" content="website" />
        <meta property="og:title" content={displayName} />
        <meta property="og:description" content={description} />
        <meta property="og:url" content={displayURL} />
        <meta property="og:image" content={displayImg} />
        <meta property="og:site_name" content="owllink" />

        {/* Twitter Meta Tags */}
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content={displayName} />
        <meta name="twitter:description" content={description} />
        <meta name="twitter:url" content={displayURL} />
        <meta name="twitter:image" content={displayImg} />
        <meta name="twitter:site" content="@OwlLinkChain" />

        {/* Schemas */}
        <script type="application/ld+json">
          {/* FAQ */}
          {JSON.stringify(personSchema)}
        </script>
      </Head>
      <PublicProfile owlLinkProfile={data.data} dns={dns} />
    </>
  );
}

// This gets called on every request
export async function getServerSideProps(context) {
  // Get path param
  const { params } = context;
  const { dns } = params;
  let profile = {};
  let profileFound = false;
  let personSchema = {};

  try {
    // If dns is available, then proceed
    if (dns) {
      // Fetch gaia URL from stacks blockchain
      const rawResponse = await fetch(
        getStacksAPIPrefix() +
          "/v2/contracts/call-read/" +
          getContractOwner() +
          "/" +
          getContractName() +
          "/get-token-uri",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            sender: getContractOwner(),
            arguments: [cvToHex(stringAsciiCV(dns))],
          }),
        }
      );
      const content = await rawResponse.json();

      // If data found on stacks blockchain
      if (content && content.okay) {
        // Get gaia address
        const gaiaProfileUrl = cvToValue(parseReadOnlyResponse(content)).value
          .value;

        try {
          // Fetch data from external API
          const res = await fetch(`${gaiaProfileUrl}?timestamp=${Date.now()}`, {
            headers: {
              "Cache-Control": "no-cache",
            },
          });
          profile = await res.json();

          // Convert to Person Schema
          convertToPersonScheme(profile);

          // Update default NFT images

          // Turn the flag on
          profileFound = true;
        } catch (error) {
          // Error reading file from gaia, but btc domain already minted.

          // Turn the flag on
          profileFound = true;
        }
      }
    }
  } catch (error) {
    // Turn the flag off
    profileFound = false;
  }

  // Prepare Person Schema
  if (profileFound) {
    const { name, description, image, socialLinks } = profile;
    let email, url;

    // List of social URLs
    let sameAs = [];
    if (socialLinks) {
      for (let key in socialLinks) {
        if (key === "email") {
          email = socialLinks[key];
        } else if (key === "website") {
          url = socialLinks[key];
        } else {
          // Get all social links
          sameAs.push(socialLinks[key]);
        }
      }
    }

    personSchema = {
      "@context": "https://schema.org",
      "@type": "Person",
    };

    if (name) {
      personSchema["name"] = name;
    }
    if (description) {
      personSchema["description"] = description;
    }
    if (image) {
      personSchema["image"] = image;
    }
    if (email) {
      personSchema["email"] = email;
    }
    if (url) {
      personSchema["url"] = url;
    }
    if (sameAs && sameAs.length > 0) {
      personSchema["sameAs"] = sameAs;
    }
  }

  // Pass data to the page via props
  return {
    props: {
      data: profile,
      dns: dns,
      profileFound: profileFound,
      personSchema,
    },
  };
}
