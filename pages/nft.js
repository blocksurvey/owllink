import Head from "next/head";
import Footer from "../components/nft/Footer";
import Menu from "../components/nft/Menu";
import NftPage from "../components/nft/NftPage";

export default function Nft() {
  // Variables
  const title =
    "Buy Owl Link NFT - The NFT that shares revenue with it&apos;s holders";
  const description =
    "Owl Link is a web 3 bio link tool. Each Owl Link NFT represents the governance ownership in the Owl Link Protocol. It is a governance token that yields from the revenue generated on the protocol.";

  return (
    <>
      {/* Header */}
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta
          name="keywords"
          content="Owl, Owl link, Link ,BlockSurvey, NFT, Crypto, Blockchain, Privacy link, One link"
        />
        <meta name="robots" content="index,follow" />

        <link rel="icon" href="/favicon.ico" />

        {/* Facebook Meta Tags */}
        <meta property="og:type" content="website" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:url" content="https://owl.link/nft" />
        <meta
          property="og:image"
          content="https://owl.link/images/nft/nft_meta_image.png"
        />
        <meta property="og:site_name" content="owllink" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />

        {/* Twitter Meta Tags */}
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={description} />
        <meta name="twitter:url" content="https://owl.link/nft" />
        <meta
          name="twitter:image"
          content="https://owl.link/images/nft/nft_meta_image.png"
        />
        <meta name="twitter:site" content="@OwlLinkChain" />

        <meta name="theme-color" content="#ffffff" />
      </Head>

      {/* Menu */}
      <Menu />

      {/* Body */}
      <NftPage />

      {/* Footer */}
      <Footer />
    </>
  );
}
