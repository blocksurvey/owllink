import { AppConfig, showConnect, UserSession } from "@stacks/connect";
import { StacksMainnet, StacksTestnet } from "@stacks/network";
import { Storage } from "@stacks/storage";
import { Constants } from "../common/constants";

const appConfig = new AppConfig(["store_write", "publish_data"]);

export const userSession = new UserSession({ appConfig });
export const storage = new Storage({ userSession });

// Set this to true if you want to use Mainnet
const stacksMainnetFlag = Constants.STACKS_MAINNET_FLAG;

export function getUserData() {
  return userSession.loadUserData();
}

export function alreadyLoggedIn() {
  if (!userSession.isUserSignedIn() && userSession.isSignInPending()) {
    userSession.handlePendingSignIn().then((userData) => {
      // Redirect to dashboard
      window.location.assign("/dashboard");
    });
  } else if (userSession && userSession.isUserSignedIn()) {
    // Redirect to dashboard
    window.location.assign("/dashboard");
  }
}

/**
 * Sign in
 */
export function authenticate() {
  // Sign up
  if (!userSession.isUserSignedIn() && !userSession.isSignInPending()) {
    showConnect({
      appDetails: {
        name: "OwlLink",
        icon: window.location.origin + "/images/logo/owllink.png",
      },
      redirectTo: "/",
      onFinish: () => {
        // Redirect to dashboard
        window.location.assign("/dashboard");
      },
      userSession: userSession,
    });
  } else if (userSession && userSession.isUserSignedIn()) {
    // Redirect to dashboard
    window.location.assign("/dashboard");
  }
}

/**
 * Sign out
 */
export function signOut() {
  // Logout
  userSession.signUserOut();

  // Redirect to dashboard
  window.location.assign("/");
}

/**
 * Save file to gaia storage
 *
 * @param {*} fileName
 * @param {*} file
 * @param {*} options
 * @returns
 */
export function putFileToGaia(fileName, file, options) {
  return storage.putFile(fileName, file, options);
}

/**
 * Get file from gaia storage
 */
export function getFileFromGaia(fileName, options) {
  // Update the fileName to make it unique using timestamp query param
  if (fileName) {
    fileName = `${fileName}?timestamp=${Date.now()}`;
  }

  return storage.getFile(fileName, options);
}

/**
 * Get stacks network type (Mainnet/Testnet)
 *
 * @returns
 */
export function getNetworkType() {
  if (stacksMainnetFlag) {
    return new StacksMainnet();
  } else {
    return new StacksTestnet();
  }
}

export function getMyStxAddress() {
  if (stacksMainnetFlag) {
    return getUserData().profile.stxAddress.mainnet;
  } else {
    return getUserData().profile.stxAddress.testnet;
  }
}

export function getStacksAPIPrefix() {
  if (stacksMainnetFlag) {
    return Constants.STACKS_MAINNET_API_URL;
  } else {
    return Constants.STACKS_TESTNET_API_URL;
  }
}

export function getBNSv2APIPrefix() {
  if (stacksMainnetFlag) {
    return Constants.BNS_V2_MAINNET_API_URL;
  } else {
    return Constants.BNS_V2_TESTNET_API_URL;
  }
}

export function getContractOwner() {
  if (stacksMainnetFlag) {
    return Constants.CONTRACT_OWNER_MAINNET;
  } else {
    return Constants.CONTRACT_OWNER_TESTNET;
  }
}

export function getContractName() {
  if (stacksMainnetFlag) {
    return Constants.CONTRACT_NAME_MAINNET;
  } else {
    return Constants.CONTRACT_NAME_TESTNET;
  }
}

export function getContractOwlLinkNFTName() {
  if (stacksMainnetFlag) {
    return Constants.CONTRACT_OWL_LINK_NFT_NAME_MAINNET;
  } else {
    return Constants.CONTRACT_OWL_LINK_NFT_NAME_TESTNET;
  }
}
